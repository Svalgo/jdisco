# README #

Java Sumo: Project for I700 - Basic Programming @ Estonian IT College
Code by: Sander Valgo, Group C11.

### What is this repository for? ###

This is a java project for making existing SumoRobot to "Dance" when a mp3 file is given as input.
Dance = to use robots controllers( LEDs, motors) to activate, randomly or based on a given .mp3
Project is not complete, but has basic functions to control the sumorobot in java language.

### How do I get set up? ###

* Summary of set up
Hardware - 
Robootikaklub's SumoRobot (https://github.com/laurivosandi/sumochip)
* Deployment instructions:

+ Find serial port name with:
`dmesg | grep tty`
+ Connect to robo with:
`picocom -b 115200 /dev/ttyACM0`
* `username: root`
* `password: chip`
+ To connect to wifi:
`nmtui`
`git pull https://Svalgo@bitbucket.org/Svalgo/jdisco.git`
+ Run Sumochip:
`sumochip_test`
+ disable sumo webserver,:
`systemctl stop sumochip`
`systemctl disable sumochip`
+ Check  if battery is connected or has power:
`axp209`


### Who do I talk to? ###

* Repo owner
Sander Valgo. Svalgo@itcollege.ee
