package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;




public class Main extends OutSumo{
    public static void infoPrint() {
        System.out.println("==========================================");
        System.out.println("Java Sumo: Project for I700 - ");
        System.out.println("Basic Programming @ Estonian IT College");
        System.out.println("Code by: Sander Valgo, Group C11");
        System.out.println("==========================================");
    }

    private static void keyListening(){
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        boolean whileLoop = true;
        while (whileLoop) {
            try {
                line = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }switch (line){
                case "w": forward(); break;
                case "a": turnLeft(); break;
                case "s": backwards(); break;
                case "d": turnRight(); break;
                case "0": allLED(1); break;
                case "9": allLED(0); break;
                case "1": ledGreen(ledBoolOnOffGREEN()); break;
                case "2": ledYellow(ledBoolOnOffYELLOW()); break;
                case "3": ledRed(ledBoolOnOffRED()); break;
                case "4": ledBlue(ledBoolOnOffBLUE()); break;
                case "x": randomMove(); break;
                case "resetB": ledBoolOnOff(); break;
                case "o": whileLoop = false; break;
            }System.out.println("");
            sleeping(200);
        }
    }

    public static void main(String[] args){
        infoPrint();
        initLed();
        System.out.println("JavaBot");
        System.out.println("move bot with WASD");
        System.out.println("All LED's On with 9, Off with 0");
        System.out.println("Led separately: green:1 Yellow:2 Red:3 Blue:4");
        System.out.println("move randomly with x");
        System.out.println("press 'o' to quit");
        keyListening();
        allLED(1);
        writerClose();
        System.out.println("goodbye from main");
    }

}