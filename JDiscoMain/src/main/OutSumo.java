package main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * here motors working inside switch of main
 * Created by svalgo on 30.11.16.
 */
public class OutSumo{
    public static void main(String[] args) {
        Main.infoPrint();
        turnLeft();
        turnRight();
        forward();
        backwards();
    }

    public static void sleeping(int sleepTime) {
        try {
            TimeUnit.MILLISECONDS.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void initLed() {
        //full path example:  /sys/class/gpio/gpio1022/value
        //path1 == beginning of path
        //path2 == pin number/LED
        //path3 == end, filename.
        String path22[] = {"1022", "1020", "1018", "1016"};

        String path1 = "/sys/class/gpio/gpio";
        String path3 = "/direction";
        try {
            for (String path2 : path22) {
                String path = path1 + path2 + path3;
                PrintWriter writerLED = new PrintWriter(path, "UTF-8");
                writerLED.print("out");
                writerLED.close();
            }
        } catch (IOException e) {
            System.out.print("You are not running it in the robot, are you?");
        }
    }
    private static void onOffLed (int inPut, String path2) {
        //full path example:  /sys/class/gpio/gpio1022/value
        //path1 == beginning of path
        //path2 == pin number/LED
        //path3 == end, filename.
        String path1 = "/sys/class/gpio/gpio";
        String path3 = "/value";
        String path = path1 + path2 + path3;
        try {
            PrintWriter writerLED = new PrintWriter(path, "UTF-8");
            writerLED.print(inPut);
            writerLED.close();
        } catch (IOException e) {
            System.out.print("You are not running it in the robot, are you?");
        }
    }

    private static boolean ledBoolean = true;
    private static boolean ledBooleanGreen = true;
    private static boolean ledBooleanYellow = true;
    private static boolean ledBooleanRed = true;
    private static boolean ledBooleanBlue = true;

    public static int ledBoolOnOff(){
        if (ledBoolean){ledBoolean = false;return 0;}
        else {ledBoolean = true;
            ledBooleanGreen = true;
            ledBooleanYellow = true;
            ledBooleanRed = true;
            ledBooleanBlue = true;
            return 1;}
    }

    public static int ledBoolOnOffGREEN(){
        if (ledBooleanGreen){ledBooleanGreen = false; return 0;}
        else {ledBooleanGreen = true;return 1;}
    }

    public static int ledBoolOnOffYELLOW(){
        if (ledBooleanYellow){ledBooleanYellow = false; return 0;}
        else {ledBooleanYellow = true;return 1;}
    }

    public static int ledBoolOnOffRED(){
        if (ledBooleanRed){ledBooleanRed = false; return 0;}
        else {ledBooleanRed = true;return 1;}
    }

    public static int ledBoolOnOffBLUE(){
        if (ledBooleanBlue){ledBooleanBlue = false; return 0;}
        else {ledBooleanBlue = true;return 1;}
    }

    public static void ledGreen(int inPut) {
        onOffLed(inPut, "1022");
    }

    public static void ledYellow(int inPut) {
        onOffLed(inPut, "1020");
    }

    public static void ledRed(int inPut) {
        onOffLed(inPut, "1018");
    }

    public static void ledBlue(int inPut) {
        onOffLed(inPut, "1016");
    }

    public static void allLED(int inPut) {
        onOffLed(inPut, "1022");
        onOffLed(inPut, "1020");
        onOffLed(inPut, "1018");
        onOffLed(inPut, "1016");
    }

    //private static String pathRight = "/sys/class/gpio/gpio119/value";
    //private static String pathLeft = "/sys/class/gpio/gpio120/value";
    private static int sleepy1 = 1;
    private static int sleepy2 = 2;
    private static int sleepyCycle = 2;

    private static int repeat = 20;


public static void randomMove(){
        for (int repeat = 0; repeat < 30; repeat++) {

            switch(ThreadLocalRandom.current().nextInt(1, 5)) {
                case 1:
                    for (;repeat < 30; repeat++) {forward();
                        System.out.println("forward");}break;
                case 2:
                    for (; repeat < 30; repeat++) {backwards();
                        System.out.println("Backwards");}break;
                case 3:
                    for (; repeat < 30; repeat++) {turnRight();
                        System.out.println("Right");}break;
                case 4:
                    for (; repeat < 30; repeat++) {turnLeft();
                        System.out.println("Left");}break;
                default: break;
            }
            ledBlue(ledBoolOnOff());
            sleeping(500);
            ledBlue(ledBoolOnOff());
            ledGreen(ledBoolOnOff());
            sleeping(500);
            ledGreen(ledBoolOnOff());
            ledRed(ledBoolOnOff());
            sleeping(500);
            ledRed(ledBoolOnOff());
            ledYellow(ledBoolOnOff());
            sleeping(500);
            ledYellow(ledBoolOnOff());
        }
    }

    public static void writerClose(){
        writerRight.close();
        writerLeft.close();
    }
    private static PrintWriter writerRight;
    private static PrintWriter writerLeft;

    static {
        try{
            writerLeft = new PrintWriter("/sys/class/gpio/gpio120/value", "UTF-8");
            writerRight = new PrintWriter("/sys/class/gpio/gpio119/value", "UTF-8");
        }catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public static void forward(){
        for (int i = 0; i < repeat; i++) {
            writerLeft.print(1); writerLeft.flush();
            writerRight.print(1); writerRight.flush();
            sleeping(sleepy1);
            writerLeft.print(0); writerLeft.flush();
            sleeping(sleepy1);
            writerRight.print(0); writerRight.flush();
            sleeping(sleepyCycle - sleepy2);
        }
    }

    public static void backwards() {
        for (int i = 0; i < repeat; i++) {
            writerRight.print(1); writerRight.flush();
            writerLeft.print(1); writerLeft.flush();
            sleeping(sleepy1);
            writerRight.print(0); writerRight.flush();
            sleeping(sleepy1);
            writerLeft.print(0); writerLeft.flush();
            sleeping(sleepyCycle - sleepy2);
        }
    }

    public static void turnRight() {
        for (int i = 0; i < repeat; i++) {
            writerRight.print(1); writerRight.flush();
            writerLeft.print(1); writerLeft.flush();
            sleeping(sleepy1);
            writerRight.print(0); writerRight.flush();
            writerLeft.print(0); writerLeft.flush();
            sleeping(sleepyCycle - sleepy1);
        }
    }


    public static void turnLeft() {
        for (int i = 0; i < repeat; i++) {
            writerRight.print(1); writerRight.flush();
            writerLeft.print(1); writerLeft.flush();
            sleeping(sleepy2);
            writerRight.print(0); writerRight.flush();
            writerLeft.print(0); writerLeft.flush();
            sleeping(sleepyCycle - sleepy2);
        }
    }
}