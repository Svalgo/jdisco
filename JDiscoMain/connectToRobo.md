File for reference of how to connect to SumoRobot: https://github.com/laurivosandi/sumochip/blob/master/doc/en/software.md
//git repo:
https://Svalgo@bitbucket.org/Svalgo/jdisco.git
#Ubuntu
// find serial port name with:
dmesg | grep tty
//connect to robo with:
picocom -b 115200 /dev/ttyACM0
username: root
password: chip
//to connect to wifi
nmtui
// ctrl-c to cancel
sumochip_test
// doesn't work in every wifi
sumochip_web

//run the robot off battery power:
systemctl start sumochip

//enable sumochip:
systemctl enable sumochip

//check battery:
axp209

//Clean shutdown:
shutdown -h now

//turn on LEDs from term:
 echo out > /sys/class/gpio/gpio1022/direction	green
 echo out > /sys/class/gpio/gpio1020/direction	yellow
 echo out > /sys/class/gpio/gpio1016/direction	blue
 echo out > /sys/class/gpio/gpio1018/direction	red
